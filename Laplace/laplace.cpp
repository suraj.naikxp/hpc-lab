#include<iostream>
#include<omp.h>
#include<stdio.h>
#include<stdlib.h>
#include<float.h>
#include<math.h>
using namespace std;
double max(double a,double b)
{
    return a>b?a:b;
}
int main(int argc,char** argv)
{
    int i,j,iter=0,maxIter;
    double tol,var=DBL_MAX,top=100.0;
    int n,n2;
    double *T,*Tnew,*Tmp;
    int items;
    cout<<"Enter the mesh size,max iterations and tolerance"<<endl;
    cin>>n>>maxIter>>tol;

    n2=n+2;
    T=(double *)calloc(n2*n2,sizeof(*T));
    Tnew = (double *)calloc(n2*n2,sizeof(*T));

    if(T==NULL || Tnew == NULL)
    {
        cout<<"Not enough memory"<<endl;
        exit(0);
    }

    for(i=1;i<=n;i++)
    {
        T[(n+1)*n2+i] = Tnew[(n+1)*n2+i] = i*top/(n+1);
        T[n2*i+n+1] = Tnew[n2*i+n+1] = i*top/(n+1);
    }

    int start = omp_get_wtime();
    #pragma omp parallel
    {
        while(var>tol && iter<=maxIter)
        {
            #pragma omp barrier                 //barrier creates a barrier, because later on in the single nowait block iter value changes causing it to break out of the loop.
            #pragma omp single
            {
                ++iter;
                var=0.0;
            }
            #pragma omp for private(j) reduction(max:var)
            for(i=1;i<=n;i++)
                for(j=1;j<=n;j++)
                {
                    Tnew[i*n2+j] = 0.25 * (T[(i-1)*n2+j] + T[(i+1)*n2+j] + T[i*n2+(j-1)] + T[i*n2+(j+1)]);
                    var = max(var,fabs(Tnew[i*n2+j] - T[i*n2+j]));
                }
            #pragma omp single nowait
            {
                Tmp=T;T=Tnew;Tnew=Tmp;
                if(iter%100 == 0)
                {
                    cout<<"Iterations and variations is "<<iter<<" and "<<var<<endl;
                }
            }
        }
    }
    double end=omp_get_wtime()-start;
    cout<<"Iterations "<<iter<<endl;
    cout<<"Variations "<<var<<endl;
    cout<<"Time "<<end<<endl;

    double flop = 5 * n * n * iter;         //There are 5 floating points operations, done over n^2 data for iter number of iterations. Note: It is flop and not flops, for flops divide by total time.

    double mflops = flop/1000000/(end);

    cout<<"flop = "<<flop<<endl;
    cout<<"Mega flops = "<<mflops<<endl;
    return 0;
}