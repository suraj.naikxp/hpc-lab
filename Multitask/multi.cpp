#include<iostream>
#include<omp.h>
#include<math.h>
using namespace std;
#define PI 3.14
int* primes,*sines;
bool isprime(long num)
{
    for(int i=2;i<num;i++)
    {
        if(num%i==0)
            return false;
    }
    return true;
}
void prime(long n)
{
    primes = new int[n];
    long num=2;
    long i =0;
    while(i<n)
    {
        if(isprime(num))
        {
            primes[i]=num;
            i++;
        }
        num++;
    }
}
void sine(long n)
{
    sines = new int[n];
    #pragma omp parallel for
    for(int i=0;i<n;i++)
    {
        int a= 0;
        for(int j=0;j<i;j++)
        {
            a=j*PI/(n-1);
            sines[i]+=sin(a);           //The equation can be reduced to sin(i), the loop j is added just to make sure it consumes more time as the other section prime takes a longer time.
        }
    }
}
int main(int argc,char** argv)
{
    long n=atoi(argv[1]);
    omp_set_num_threads(atoi(argv[2]));

    double t1,t2;
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            t1=omp_get_wtime();
            prime(n);
            t1=omp_get_wtime()-t1;
        }
        #pragma omp section
        {
            t2=omp_get_wtime();
            sine(n);
            t2=omp_get_wtime()-t2;
        }
    }
    cout<<"Time for primes is "<<t1<<endl;
    cout<<primes[9]<<endl;
    cout<<"Time for sines is "<<t2<<endl;
    return 0;
}