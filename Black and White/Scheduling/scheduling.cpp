#include<iostream>
#include<omp.h>
#include<gd.h>
#include<cstdio>
using namespace std;
int main(int argc,char** argv)
{
    gdImagePtr img;
    int w,h;
    FILE* fp;
    fp=fopen(argv[1],"r");
    img = gdImageCreateFromPng(fp);
    w = gdImageSX(img);
    h = gdImageSY(img);

    int x,y;
    omp_set_num_threads(atoi(argv[3]));
    double time = omp_get_wtime();
    #pragma omp parallel for private(y) schedule(guided,100)   // Try for different scheduling techniques: Guided,Static,Dynamic and Default, also experiment with chunk sizes
    for(x=0;x<w;x++)
    {
        for(y=0;y<h;y++)
        {
            int tid = omp_get_thread_num();
            int color=gdImageGetPixel(img,x,y);
            if(tid == 0)
            {
                color = gdImageColorAllocate(img,255,0,0);
                gdImageSetPixel(img,x,y,color);
            }
            if(tid == 1)
            {
                color = gdImageColorAllocate(img,0,255,0);
                gdImageSetPixel(img,x,y,color);
            }
            if(tid == 2)
            {
                color = gdImageColorAllocate(img,0,0,255);
                gdImageSetPixel(img,x,y,color);
            }
            if(tid == 3)
            {
                color = gdImageColorAllocate(img,0,0,0);
                gdImageSetPixel(img,x,y,color);
            }
        }
    }
    time = omp_get_wtime()-time;
    fp = fopen(argv[2],"w");
    gdImagePng(img,fp);
    gdImageDestroy(img);
    fclose(fp);
    cout<<"The time taken is "<<time<<endl;
    return 0;
}