#include<iostream>
#include<omp.h>
#include<gd.h>
#include<cstdio>
using namespace std;
int main(int argc,char** argv)
{
    gdImagePtr img;
    int w,h;
    FILE* fp;
    fp=fopen(argv[1],"r");
    img = gdImageCreateFromPng(fp);
    w = gdImageSX(img);
    h = gdImageSY(img);

    int x,y;
    omp_set_num_threads(atoi(argv[3]));
    double time = omp_get_wtime();
    #pragma omp parallel for private(y)     // y is made private to ensure value doesn't change in between execution causing wrong pixel to be colored
    for(x=0;x<w;x++)
    {
        for(y=0;y<h;y++)
        {
            int color=gdImageGetPixel(img,x,y);
            int red = gdImageRed(img,color);
            int green = gdImageGreen(img,color);
            int blue = gdImageBlue(img,color);
            int temp = (red+green+blue)/3;
            red = green = blue = temp;
            color = gdImageColorAllocate(img,red,green,blue);
            gdImageSetPixel(img,x,y,color);
        }
    }
    time = omp_get_wtime()-time;
    fp = fopen(argv[2],"w");
    gdImagePng(img,fp);
    gdImageDestroy(img);
    fclose(fp);
    cout<<"The time taken is "<<time<<endl;
    return 0;
}