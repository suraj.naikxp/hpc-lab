# HPC LAB

This repo contains codes in C,C++ and CUDA of HPC LAB[12CS71] of 2018-2019 batch 7th semester CSE course.



I have written a few of the codes on my own, but a few of them have been contributed by my friend Srinivas S T [https://github.com/srinivasst], especially the CUDA, MPI and the word search program. Shoutout to ST.

Point Classification : Courtesy of Shreyas A R [https://www.linkedin.com/in/shreyas-ar-756146141/]