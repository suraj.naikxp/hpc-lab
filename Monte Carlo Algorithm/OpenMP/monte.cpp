#include<iostream>
#include<omp.h>
#include<cstdlib>
using namespace std;
int main(int argc,char** argv)
{
    cout<<"Enter the number of points"<<endl;
    int n;
    cin>>n;
    int threads;
    cout<<"Enter the number of threads"<<endl;
    cin>>threads;
    
    omp_set_num_threads(threads);

    int count;
    unsigned int seed=0;
    double time=omp_get_wtime();
    #pragma omp parallel for firstprivate(seed) reduction(+:count)
    for(int i=0;i<n;i++)
    {
        double x = (double)rand_r(&seed)/RAND_MAX;              //rand_r should be used instead of rand() as it is thread safe.
        double y = (double)rand_r(&seed)/RAND_MAX;
        double z=x*x+y*y;
        if(z<=1)
            count++;
    }
    time = omp_get_wtime()-time;
    double pi = (double)count/n*4;
    cout<<"Pi value is "<<pi<<endl;
    cout<<"Total time taken is "<<time<<endl;
    return 0;
}