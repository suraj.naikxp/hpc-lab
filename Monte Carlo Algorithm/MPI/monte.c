#include<mpi.h>
#include<stdio.h>
#include<stdlib.h>
int main(int argc,char** argv)
{
    int rank,size,count,reduced_count=0,seed=0;
    double x,y,z,pi,time;
    long n,i;
    n=atol(argv[1]);
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    time=MPI_Wtime();
    for(i=0;i<(n/size);i++)
    {
        x=(double)rand_r(&seed)/RAND_MAX;
        y=(double)rand_r(&seed)/RAND_MAX;
        z=x*x+y*y;
        if(z<=1)
            count++;
    } 
    reduced_count = 0;
    MPI_Reduce(&count,&reduced_count,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
    if(rank==0)
    {
        time=MPI_Wtime()-time;
        pi=(double)reduced_count/n*4;
        printf("PI value is %lf\n",pi);
        printf("Time taken is %lf\n",time);
    }   
    MPI_Finalize();
}