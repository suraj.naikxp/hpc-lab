#include<iostream>
#include<omp.h>
#include<gd.h>
#include<cstdio>
using namespace std;
int main(int argc,char** argv)
{
    gdImagePtr img;
    int w,h;
    FILE* fp;
    fp=fopen(argv[1],"r");
    img = gdImageCreateFromPng(fp);
    w = gdImageSX(img);
    h = gdImageSY(img);

    int x,y;
    omp_set_num_threads(atoi(argv[3]));
    double time = omp_get_wtime();
    #pragma omp parallel for                //During data scope use  "#pragma omp parallel for private(y)" and remove the critical section
    for(x=0;x<w;x++)
    {
        #pragma omp critical
        for(y=0;y<h;y++)
        {
            int color=gdImageGetPixel(img,x,y);
            int red = 255 - gdImageRed(img,color);
            int green = 255 - gdImageGreen(img,color);
            int blue = 255 - gdImageBlue(img,color);
            color = gdImageColorAllocate(img,red,green,blue);
            gdImageSetPixel(img,x,y,color);
        }
    }
    time = omp_get_wtime()-time;
    fp = fopen(argv[2],"w");
    gdImagePng(img,fp);
    gdImageDestroy(img);
    fclose(fp);
    cout<<"The time taken is "<<time<<endl;
    return 0;
}