#include<stdlib.h>
#include<stdio.h>
#include<cmath>
void VectorAddS(int *A, int *B, int *C, int N)
{
    for(int i=0;i<N;i++)
    {
        C[i]=A[i]+B[i];
    }
}
__global__ void VectorAddP(int *A, int *B, int *C, int N)
{
    int id = blockIdx.x*blockDim.x + threadIdx.x;
    if(id<N)
    {
        C[id]=A[id]+B[id];
    }
}
int main(int argc, char const *argv[])
{
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    int N = atoi(argv[1]);
    int *hA = new int[N];
    int *hB = new int[N];
    int *hCp = new int[N];
    int *hCs = new int[N];
    int *dA;
    int *dB;
    int *dC;
    int size=N*sizeof(int);
    for(int i=0;i<N;i++)
    {
        hA[i]=rand()%100;
        hB[i]=rand()%100;
    }
    cudaMalloc(&dA,size);
    cudaMalloc(&dB,size);
    cudaMalloc(&dC,size);
    cudaMemcpy(dA,hA,size,cudaMemcpyHostToDevice);
    cudaMemcpy(dB,hB,size,cudaMemcpyHostToDevice);
    int blockSize=1024;                                     //reduce the block size according to your CUDA cores
    int gridSize=ceil((float)N/1024);
    cudaEventRecord(start);
    VectorAddP<<<gridSize,blockSize>>>(dA,dB,dC,N);
    cudaEventRecord(stop);
    cudaMemcpy(hCp,dC,size,cudaMemcpyDeviceToHost);
    float milliseconds = 0;

    cudaEventElapsedTime(&milliseconds, start, stop);
    bool flag =true;
    bool skipVerify=false;
    if(skipVerify!=true)
    {
        VectorAddS(hA,hB,hCs,N);
        for(int i=0;i<N;i++)
        {
            if(hCp[i]!=hCs[i])
            {
                flag=false;
                break;
            }
        }
    }
    if(flag || skipVerify)
    {
        printf("Output Verified\n");
        printf("Time = %f",milliseconds/1000);
    }
}