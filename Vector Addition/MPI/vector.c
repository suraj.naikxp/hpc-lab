#include<stdio.h>
#include<mpi.h>
#include<stdlib.h>
int main(int argc,char** argv)
{
    int rank,np;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&np);
    int *a,*b,*c,*ap,*bp,*cp;
    long n=atol(argv[1]);
    long nper = n/np;

    if(n%np!=0)
        nper++;

    a=(int *)malloc(sizeof(int)*n);
    b=(int *)malloc(sizeof(int)*n);
    c=(int *)malloc(sizeof(int)*n);

    for(int i=0;i<n;i++)
    {
        a[i]=i;
        b[i]=i;
    }
    ap=(int *)malloc(sizeof(int)*nper);
    bp=(int *)malloc(sizeof(int)*nper);
    cp=(int *)malloc(sizeof(int)*nper);

    double t1;
    if(rank==0)
        t1=MPI_Wtime();

    MPI_Scatter(a,nper,MPI_INT,ap,nper,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Scatter(b,nper,MPI_INT,bp,nper,MPI_INT,0,MPI_COMM_WORLD);

    for(int i=0;i<nper;i++)
        cp[i]=ap[i]+bp[i];

    MPI_Gather(cp,nper,MPI_INT,c,nper,MPI_INT,0,MPI_COMM_WORLD);

    if(rank==0)
    {
        double t2=MPI_Wtime()-t1;
        printf("\nTime = %lf\n",t2);
    }
    MPI_Finalize();
}
