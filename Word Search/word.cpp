#include<iostream>
#include<stdio.h>
#include<omp.h>
#include<string.h>
#include<ctype.h>
#include<math.h>
using namespace std;
int main(int argc,char** argv)
{
    char words[][10] = {"the","around","graphics","from","by","be","any","mount","hello","word"};
    int total[10] = {0};
    int threads = atoi(argv[1]);
    omp_set_num_threads(threads);
    const char* input = "words4.txt";
    double time = omp_get_wtime();
    #pragma omp parallel
    {
        int count[10] = {0};
        int tnum = omp_get_thread_num();
        int skip;
        FILE* fp = fopen(input,"r");
        fseek(fp,0,SEEK_END);
        int size = ftell(fp);
        int block = ceil((double)size/threads);

        if(tnum!=0)
        {
            skip = -1;
            fseek(fp,tnum*block-1,SEEK_SET);
        }
        else
        {
            skip = 0;
            fseek(fp,0,SEEK_SET);
        }

        bool check =true;
        int i=0;
        char ch;
        char arr[100];
        while((ch=fgetc(fp))!=EOF)
        {
            if(tnum!=0 && check)
            {
                if(isspace(ch))
                    check = false;
                skip++;

                if(skip>=block)
                    break;
                    
                continue;
            }
            arr[i++] = ch;
            skip++;
            if(isspace(ch))
            {
                arr[i-1] = 0;
                for(int j=0;j<10;j++)
                    count[j]+=(strcmp(arr,words[j])?0:1);
                i=0;
            }

            if(skip>=block && isspace(ch))
                break;
        }
        fclose(fp);
        #pragma omp critical
        {
            for(int j=0;j<10;j++)
            {
                total[j]+=count[j];
            }
        }
    }
    time = omp_get_wtime()-time;
    cout<<"Time is "<<time<<endl;
    for(int j=0;j<10;j++)
    {
        cout<<words[j]<<" "<<total[j]<<endl;
    }
    return 0;
}