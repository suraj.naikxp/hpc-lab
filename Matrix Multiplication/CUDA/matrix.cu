#include<iostream>
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
__global__ void multiply(int *A,int* B,int* C,int n)
{
    int row = blockIdx.y*blockDim.y + threadIdx.y;
    int col = blockIdx.x*blockDim.x + threadIdx.y;

    float sum = 0.0;
    if(row<n && col<n)
    {
        for(int k=0;k<n;k++)
        {
            sum+=A[row*n+k]*B[k*n+col];
        }
        C[row*n+col]=sum;
    }
}
int main(int argc,char** argv)
{
    int *hA,*hB,*hC,*dA,*dB,*dC;
    cudaEvent_t start,stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    
    int n=atoi(argv[1]);
    
    hA= new int[n*n];
    hB= new int[n*n];
    hC = new int[n*n];

    for(int i=0;i<(n*n);i++)
    {
        hA[i]=rand()%100;
        hB[i]= rand()%100;
    }
    int size = n*n*sizeof(int);
    
    cudaMalloc(&dA,size);
    cudaMalloc(&dB,size);
    cudaMalloc(&dC,size);

    dim3 blockSize(32,32);             //32x32 is 1024 which is almost the number of cuda cores my gpu has,, reduce it according to your gpu configuration like 16,16 or 8,8
    int GS = ceil((float)n/32);
    dim3 gridSize(GS,GS);

    cudaMemcpy(dA,hA,size,cudaMemcpyHostToDevice);
    cudaMemcpy(dB,hB,size,cudaMemcpyHostToDevice);
    
    cudaEventRecord(start);
    multiply<<<gridSize,blockSize>>>(dA,dB,dC,n);
    cudaEventRecord(stop);

    cudaMemcpy(hC,dC,size,cudaMemcpyDeviceToHost);

    double time = 0;
    cudaEventElapsedTime(&time,start,stop);
    cout<<"Time in ms "<<time<<endl;

    return 0;
}