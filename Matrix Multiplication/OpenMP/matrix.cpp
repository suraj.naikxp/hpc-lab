#include<iostream>
#include<cmath>
#include<cstdlib>
#include<omp.h>
using namespace std;
void matrixMulParallel(int* A,int* B,int* C,int n)
{
    #pragma omp parallel for
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        {
            C[i*n+j]=0;
            for(int k=0;k<n;k++)
            {
                C[i*n+j]+=A[i*n+k]*B[k*n+j];
            }
        }
    }
}
void matrixMulSerial(int* A,int* B,int* C,int n)
{
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        {
            C[i*n+j]=0;
            for(int k=0;k<n;k++)
            {
                C[i*n+j]+=A[i*n+k]*B[k*n+j];
            }
        }
    }
}
int main(int argc,char** argv)
{
    int *A,*B,*CS,*CP;
    int n=atoi(argv[1]);
    omp_set_num_threads(atoi(argv[2]));

    A=new int[n*n];
    B=new int[n*n];
    CS=new int[n*n];
    CP=new int[n*n];

    for(int i=0;i<n*n;i++)
    {
        A[i] = rand()%100;
        B[i] = rand()%100;
    }

    double time1 = omp_get_wtime();
    matrixMulSerial(A,B,CS,n);
    time1 = omp_get_wtime()-time1;

    double time2 = omp_get_wtime();
    matrixMulParallel(A,B,CP,n);
    time2=omp_get_wtime()-time2;

    bool flag=true;
    for(int i=0;i<n*n;i++)
    {
        if(CS[i]!=CP[i])
        {
            flag=false;
        }
    }

    if(flag)
    {
        cout<<"Time for serial is "<<time1<<endl;
        cout<<"Time for parallel is "<<time2<<endl;
    }
    else
    {
        cout<<"Serial is not equal to parallel"<<endl;
    }
    return 0;
}