#include<stdio.h>
#include<mpi.h>
#include<string.h>
int main(int argc,char** argv)
{
    int rank,size;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Status status;
    char message [][50] = {"Hello CSE","HELLO RVCE","HELLO Suraj"};
    char mess[50];
    int buffer = 50;
    if(rank == 0)
    {
        for(int i=1;i<size;i++)
        {
            MPI_Recv(mess,buffer,MPI_CHAR,i,0,MPI_COMM_WORLD,&status);
            printf("Received message is %s from rank %d\n",mess,i);
        }
    }
    else
    {
        MPI_Send(message[rank-1],buffer,MPI_CHAR,0,0,MPI_COMM_WORLD);
    }
    MPI_Finalize();
}