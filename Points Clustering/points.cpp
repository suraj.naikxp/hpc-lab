#include<iostream>
#include<math.h>
#include<time.h>
#include<stdlib.h>
#include<omp.h>
using namespace std;
long** points;
int count[4];
int mean[4];
void cluster(long n)
{
    #pragma omp parallel for
    for(long i = 4;i<n;i++)
    {
        int min = 100000;
        int cur = 0;
        int index = -1;
        for(int k=0;k<4;k++)
        {
            cur = fabs(points[i][0] - mean[k]);
            if(cur<min)
            {
                min = cur;
                index = k;
            }
        }
        points[i][1] = index;
        #pragma omp critical
        {
            mean[index] = (mean[index] * count[index] + points[i][0])/(count[index]+1);
            count[index]++;
        }
    }
}
void populate(long n)
{
    points = new long*[n];
    for(int i=0;i<n;i++)
    {
        points[i] = new long[2];
    }

    srand(time(0));
    for(int i=0;i<n;i++)
    {
        points[i][0] = rand()%100;
    }

    for(int i=0;i<4;i++)
    {
        count[i] = 1;
        mean[i] = points[i][0];
        points[i][1] = i;
    }
}
int main(int argc,char** argv)
{
    long n = atol(argv[1]);
    omp_set_num_threads(atoi(argv[2]));

    populate(n);

    double time  = omp_get_wtime();
    cluster(n);
    time = omp_get_wtime() - time;

    cout<<"Time is "<<time<<endl;
    for(int i=0;i<4;i++)
    {
        cout<<count[i]<<endl;
    }
}