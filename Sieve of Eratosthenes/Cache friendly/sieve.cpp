#include<iostream>
#include<omp.h>
#include<stdlib.h>
#include<string.h>
#include<cmath>
using namespace std;
int strike(int* composite,long i,long stride,long limit)
{
    for(;i<=limit;i+=stride)
    {
        composite[i] = 1;
    }
    return i;
}
int main(int argc,char** argv)
{
    long n = atol(argv[1]);
    int* composite = new int[n+1];
    long m = (long)ceil((double)sqrt(n));
    long* striker = new long[m];
    long* factor = new long[m];
    long n_factor = 0;
    memset(composite,0,n*sizeof(int));
    long count = 0;
    double time = omp_get_wtime();
    for(long i=2;i<=m;i++)
    {
        if(composite[i]==0)
        {
            count++;
            striker[n_factor] = strike(composite,2*i,i,m);
            factor[n_factor++] = i;
        }
    }
    for(long window = m+1;window<=n;window+=m)
    {
        long limit = min((window+m-1),n);
        for(long k=0;k<n_factor;k++)
        {
            striker[k] = strike(composite,striker[k],factor[k],limit);
        }
        for(long i=window;i<=limit;i++)
        {
            if(composite[i]==0)
                count++;
        }
    }
    time = omp_get_wtime()-time;

    cout<<"The number of prime number is "<<count<<endl;
    cout<<"The total time taken is "<<time<<endl;

    return 0;
}