#include<iostream>
#include<omp.h>
#include<cstdlib>
#include<cmath>
#include<string.h>
using namespace std;
void strike(bool* composite,long i,long stride,long limit)
{
    for(;i<=limit;i+=stride)
        composite[i] = true;
}
int main(int argc,char** argv)
{
    long n=atol(argv[1]);
    bool* composite = new bool[n+1];
    int m =ceil((float)sqrt(n));    
    memset(composite,0,n);

    double time = omp_get_wtime();

    long count =0;
    for(int i=2;i<=m;i++)
    {
        if(!composite[i])
        {
            count++;
            strike(composite,2*i,i,n);
        }
    }
    for(long i=m+1;i<=n;i++)
    {
        if(!composite[i])
            count++;
    }
    time = omp_get_wtime()-time;
    cout<<"Number of prime numbers is "<<count<<endl;
    cout<<"Total time taken is "<<time<<endl;
}