#include<iostream>
#include<omp.h>
#include<stdlib.h>
#include<string.h>
#include<cmath>
using namespace std;
int min(int a,int b)
{
    return a<b?a:b;
}
inline long strike(bool* composite,long i,long stride,long limit)
{
    for(;i<=limit;i+=stride)
    {
        composite[i] = true;
    }
    return i;
}
int main(int argc,char** argv)
{
    long n = atol(argv[1]);
    omp_set_num_threads(atoi(argv[2]));
    long m = (long)ceil((double)sqrt(n));
    long* factor = new long[m];
    long n_factor = 0;
    long count=0;
    double time = omp_get_wtime();
    #pragma omp parallel 
    {
        bool* composite = new bool[m+1];
        long* striker = new long[m];
        #pragma omp single
        {
            memset(composite,0,m);
            for(long i=2;i<=m;i++)
            {
                if(!composite[i])
                {
                    count++;
                    strike(composite,2*i,i,m);
                    factor[n_factor++] = i;
                }
            }
        }
        long base=-1;
        #pragma omp for reduction(+: count)
        for(long window = m+1;window<=n;window+=m)
        {
            memset(composite,0,m);
            if(base!=window)
            {
                base=window;
                for(long k=0;k<n_factor;k++)
                {
                    striker[k]=(base+factor[k]-1)/factor[k]*factor[k]-base;
                }
            }
            long limit = min((window+m-1),n)-base;
            for(long k=0;k<n_factor;k++)
            {
                striker[k] = strike(composite,striker[k],factor[k],limit)-m;
            }
            for(long i=0;i<=limit;i++)
            {
                if(!composite[i])
                    count++;
            }
            base+=m;
        }
    }
    time = omp_get_wtime()-time;

    cout<<"The number of prime number is "<<count<<endl;
    cout<<"The total time taken is "<<time<<endl;

    return 0;
}